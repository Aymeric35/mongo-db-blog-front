import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../models/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  // ARTICLES: any[] = [];

  constructor() { }


  public async getArticles() {
    const response = await axios.get("http://localhost:8080/api/articles");
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  public addArticle(article: Article) {
    return axios.post("http://localhost:8080/api/articles", {
      title: article.title,
      author: article.author,
      summary: article.summary,
      content: article.content
    })
      .then(function (response) {
        console.log(response.data);
        return response.data
      })
      .catch(function (error) {
        console.log(error);
        return error;
      });
  }


}
