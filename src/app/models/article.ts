export interface Article {
    title: string,
    author: string,
    summary: string,
    content: string
}