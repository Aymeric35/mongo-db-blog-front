import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  articles: Article[] = [];

  constructor(private articleService: ArticleService) { }

  async displayArticles() {
    this.articles = await this.articleService.getArticles();
  }

  ngOnInit(): void {
    this.displayArticles();
  }

}
